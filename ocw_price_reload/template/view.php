<form action="" class="form_settings" style="    margin-top: 40px;">
    <div class="settings_price">
        <?php print_r(get_all_fields()); ?>
    </div>

</form>
<div>
    <button type="button" style="background: transparent; border: none; font-size: 39px;  margin-left: 20px; line-height: 76px; display: block; float: right;" class="add_new">+</button>
    <button type="button" style="background: transparent; font-size: 20px; color: #000; border: solid 0.5px;  padding: 8px; margin-top: 20px;" class="save">Save</button>
</div>

<script>
    $('body').on('click','.delete_this',function () {
        $(this).parent().remove();
    })
    $('.add_new').click(function () {

        $('.inner').last().clone().appendTo('.settings_price');
        $('.inner').last().find('input').val('');
    })

    jQuery(document).ready(function ($) {

        $('.save').click(function () {
            var form = $('.form_settings').serialize();
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: 'action=add_to_db_prices&' + form,
                success: function (data) {
                    alert('All setting has been saved');
                    document.location.reload(true);
                }
            });
        })

    });
</script>