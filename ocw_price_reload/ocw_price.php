<?php
/*
Plugin Name: OCW Price
Version: 1.0
Author: OCW team
*/


if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'הנחות ומבצעים',
        'menu_title'	=> 'הנחות ומבצעים',
        'menu_slug' 	=> 'ocw-discounts',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));


}

require_once 'functions.php';



