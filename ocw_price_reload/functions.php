<?php
add_action('woocommerce_init', 'ocw_init_functions', 10, 1);
function ocw_init_functions()
{
    $i = 0;
    $i++;
    $data_ocw = new WC_Session_Handler();
    if (!is_admin()) {
        ocw_remove_coupon();
        ocw_apply_discount();
    }

}

function ocw_get_discount_amount()
{
    global $woocommerce;
    $coupon_discount_totals = $woocommerce->cart->coupon_discount_totals;
    $discount = false;
    $increment = get_option('options_added_discount');
    $all_cart = get_option('options_all_cart');
    $discount_data = get_option('options_discount_table');
    if ($discount_data) {
        for ($i = 0; $i < $discount_data; $i++) {
            $discount_table[$i]['amount'] = get_option('options_discount_table_' . $i . '_amount');
            $discount_table[$i]['percent'] = get_option('options_discount_table_' . $i . '_percent');
        }
    }
    usort($discount_table, 'amount_compare');
    $all = 0;
    if (!$woocommerce->cart) {
        return;
    }
    foreach ($woocommerce->cart->get_cart() as $cart_item_key => $values) {
        $_product = $values['data'];
        if (!$_product->is_on_sale()) {
            $all++;
        } else {
            continue;
        }
        $product_price[] = get_option('woocommerce_tax_display_cart') == 'excl' ? $_product->get_price_excluding_tax() : $_product->get_price_including_tax(); /*Store all product price from cart items in Array */
    }
    $lowestprice = ($all_cart) ? array_sum($product_price) : min($product_price);
    foreach ($discount_table as $r) {
        if ($all < $r['amount']) continue;
        $discount = round($discount + ($lowestprice / 100 * $r['percent']));
        if (!$increment) break;
    }
    return $discount;
}

function ocw_create_coupon($discount)
{
    $data_ocw = new WC_Session_Handler();
    $session_id_ocw = $data_ocw->get_session_cookie()[0];
    $code = uniqid();
    $code = intval($code) + 1;
    $coupon_code = $code;
    $amount = $discount;
    $discount_type = 'fixed_cart';
    $coupon = array(
        'post_title' => $session_id_ocw.'sale_sys' . $coupon_code . '---' . $amount . '' . rand(1, 15),
        'post_content' => '',
        'post_status' => 'publish',
        'post_author' => 1,
        'post_type' => 'shop_coupon'
    );
    $new_coupon_id = wp_insert_post($coupon);
    $sys_coupon = $discount;
    setcookie("sys_coupon_id", "", time() - 3600);
    setcookie("sys_coupon", "", time() - 3600);
    setcookie("sys_coupon", $sys_coupon);
    setcookie("sys_coupon_id", $new_coupon_id);
    update_post_meta($new_coupon_id, 'discount_type', $discount_type);
    update_post_meta($new_coupon_id, 'coupon_amount', $amount);
    update_post_meta($new_coupon_id, 'individual_use', 'no');
    update_post_meta($new_coupon_id, 'product_ids', '');
    update_post_meta($new_coupon_id, 'exclude_product_ids', '');
    update_post_meta($new_coupon_id, 'usage_limit', '1');
    update_post_meta($new_coupon_id, 'usage_limit_per_user', '1');
    update_post_meta($new_coupon_id, 'expiry_date', '');
    update_post_meta($new_coupon_id, 'apply_before_tax', 'yes');
    update_post_meta($new_coupon_id, 'free_shipping', 'no');
    update_option('ocw_unique_code_coupon', $code);
    add_option('delete_coupon_' . $code, $new_coupon_id, '', false);
    return $coupon["post_title"];
}

function ocw_apply_discount()
{
    global $woocommerce;

    $discount = ocw_get_discount_amount();
    if (isset($_COOKIE['sys_coupon'])) {
        $applied_coupon = $_COOKIE['sys_coupon'];
        if (isset($applied_coupon) && $applied_coupon != '') {
            $sys_coupon_val = round($applied_coupon);
        }
        $discount = ocw_get_discount_amount($woocommerce);
        if (floatval($sys_coupon_val) == $discount) return;
    }
    if ($discount) {
        $coupon = ocw_create_coupon($discount);
        $coupon_discount_totals = $woocommerce->cart->coupon_discount_totals;
        if (empty($coupon_discount_totals)) {
			$no_repeat = 1;
            $woocommerce->cart->add_discount(sanitize_text_field($coupon));
        }


    }
    return;
}

function ocw_get_applied_coupon()
{
    if (isset($_COOKIE['sys_coupon'])) {
        $sys_coupon_val = $_COOKIE['sys_coupon'];
    }
    return $sys_coupon_val;
}

function ocw_remove_coupon()
{
    global $woocommerce;
    if (!$woocommerce->cart) {
        return;
    }
    if (empty($_COOKIE['sys_coupon_id'])) return;
    $ocw_applied_coupon_val = $_COOKIE['sys_coupon'];
    $discount = ocw_get_discount_amount();

    if (!$discount) {

        ocw_remove_and_delete_coupon($ocw_applied_coupon_val);
    }
    $code = get_option('ocw_unique_code_coupon', 1);

    if ((floatval($ocw_applied_coupon_val) != $discount)) {
        ocw_remove_and_delete_coupon($code);
    }

}

function ocw_remove_and_delete_coupon($code)
{
    $data_ocw = new WC_Session_Handler();
    $session_id_ocw = $data_ocw->get_session_cookie()[0];
    global $woocommerce;
    if (!$woocommerce->cart) {
        return;
    }
    $woocommerce->cart->remove_coupons(sanitize_text_field($code));
    $coupon_id = $_COOKIE['sys_coupon_id'];
    if ($coupon_id) {
        wp_delete_post($coupon_id, 1);
    }
    setcookie("sys_coupon_id", "", time() - 3600);
    setcookie("sys_coupon", "", time() - 3600);
    query_posts(array(
        'post_type' => 'shop_coupon',
        'showposts' => 1000
    ) );

    while (have_posts()) : the_post();
        $pos = strpos(get_the_title(), $session_id_ocw.'sale_sys', 0);

        if($pos === 0){
            wp_delete_post(get_the_ID(), 1);
        }
    endwhile;
    wp_reset_query();
}

function amount_compare($a, $b)
{

    return $b['amount'] - $a['amount'];
}