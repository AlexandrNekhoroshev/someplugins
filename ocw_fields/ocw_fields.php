<?php
/**
 * Plugin Name: OCW Fields
 * Description: Add colors and images in attributes.
 * Version: 1.0
 * Author: Alex
 */



    $taxname = 'pa_color';

    add_action("{$taxname}_add_form_fields", 'add_new_custom_fields');
    add_action("{$taxname}_edit_form_fields", 'edit_new_custom_fields');
    add_action("create_{$taxname}", 'save_custom_taxonomy_meta');
    add_action("edited_{$taxname}", 'save_custom_taxonomy_meta');


    function edit_new_custom_fields( $term ) {
        ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label>צבע</label></th>
            <td>
                <input type="text" class="color_change" name="extra[color]" value="<?php echo (esc_attr( get_term_meta( $term->term_id, 'color', 1 ) )) ? esc_attr( get_term_meta( $term->term_id, 'color', 1 ) ): '' ?>">
                <input type="color" class="color_pick"  value="<?php echo (esc_attr( get_term_meta( $term->term_id, 'color', 1 ) )) ? esc_attr( get_term_meta( $term->term_id, 'color', 1 ) ): '' ?>"><br />
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label>תמונה</label></th>
            <td>
                <input type="text" class="widefat custom_media_url" name="extra[image]" value="<?php echo esc_attr( get_term_meta( $term->term_id, 'image', 1 ) ) ?>"><br />
                <input type="button" value="<?php _e( 'Upload Image', 'iuw' ); ?>" class="button custom_media_upload" id="custom_image_uploader"/>
            </td>
        </tr>

        <?php
    }

    function add_new_custom_fields( $taxonomy_slug ){
        ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label>Color</label></th>
            <td>
                <input type="color" name="extra[color]" value=""><br />
                <span class="description">Color</span>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label>Image</label></th>
            <td>
                <input type="text" class="widefat custom_media_url" name="extra[image]" id="" value=""><br />
                <input type="button" value="<?php _e( 'Upload Image', 'iuw' ); ?>" class="button custom_media_upload" id="custom_image_uploader"/>
            </td>
        </tr>
        <?php
    }

    function save_custom_taxonomy_meta( $term_id ) {
        if ( ! isset($_POST['extra']) ) return;
        if ( ! current_user_can('edit_term', $term_id) ) return;
        if (
            ! wp_verify_nonce( $_POST['_wpnonce'], "update-tag_$term_id" ) && // wp_nonce_field( 'update-tag_' . $tag_ID );
            ! wp_verify_nonce( $_POST['_wpnonce_add-tag'], "add-tag" ) // wp_nonce_field('add-tag', '_wpnonce_add-tag');
        ) return;

        // Все ОК! Теперь, нужно сохранить/удалить данные
        $extra = wp_unslash($_POST['extra']);

        foreach( $extra as $key => $val ){
            // проверка ключа
            $_key = sanitize_key( $key );
            if( $_key !== $key ) wp_die( 'bad key'. esc_html($key) );

            // очистка
            if( $_key === 'tag_posts_shortcode_links' )
                $val = sanitize_textarea_field( strip_tags($val) );
            else
                $val = sanitize_text_field( $val );

            // сохранение
            if( ! $val )
                delete_term_meta( $term_id, $_key );
            else
                update_term_meta( $term_id, $_key, $val );
        }

        return $term_id;
    }
    function iuw_wdScript(){
        wp_enqueue_media();
        wp_enqueue_script('adsScript', plugins_url( '/js/common.js' , __FILE__ ),array(),true);
    }
    add_action('admin_enqueue_scripts', 'iuw_wdScript');
