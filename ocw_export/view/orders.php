<?php
	if (isset($_POST['save_orders']) && ! empty($_POST['save_orders'])) {

		$glue = (isset($_POST['glue']) && ! empty($_POST['glue']) ? $_POST['glue'] : ';');
		$glue = chr(0x09).chr(0x00);

		$agrs   = array(
			'posts_per_page' => 9999,
		);
		$orders = wc_get_orders($agrs);// get_users($users_args);

		$fields = [
			'id',
			'status',
			'date_created'  => ['date'],
			'date_modified' => ['date'],
			'discount_total',
			'shipping_total',
			'total',
			'customer_id',
			'payment_method',
			'customer_note',
			'date_paid',
			'date_completed',
//			'billing'       => [
//				'first_name',
//				'last_name',
//				'address_1',
//				'city',
//				'postcode',
//				'country',
//				'email',
//				'phone',
//			],
			'shipping'      => [
				'first_name',
				'last_name',
				'address_1',
				'city',
				'postcode',
//				'country',
				'email',
				'phone',
			]
		];
		$temp   = [];
		foreach ($fields as $key => $field) {
			if (is_array($field)) {
				foreach ($field as $subfield) {
					$temp[] = $key.'_'.$subfield;
				}
			} else {
				$temp[] = $field;
			}
		}
		$this->msg = implode($glue, array_merge($temp, ['data'])).PHP_EOL;

		foreach ($orders as $order_key => $order) {

			$order_data = $order->get_data();
			$temp       = [];

			foreach ($fields as $key => $field) {
				if ($order_data[ $key ] instanceof WC_DateTime) {
					$temp[] = $order_data[ $key ]->date('d.m.Y h:i');
				} else {
					if (is_array($field)) {
						foreach ($field as $subfield) {
							if (is_object($field)) {
								$temp[] = $order_data[ $key ]->$subfield;
							} else {
								$temp[] = $order_data[ $key ][ $subfield ];
							}
						}
					} else {
						$temp[] = $order_data[ $field ];
					}
				}
				end($temp);         // move the internal pointer to the end of the array
				$key          = key($temp);
				$temp[ $key ] = '"'.$temp[ $key ].'"';
			}
			$items = [];
			$order = $order->get_items();
			foreach ($order as $item) {
				$item    = new WC_Order_Item_Product($item);
				$items[] = $item->get_name();
			}
			$temp[]    = implode(' / ', $items);
			$this->msg .= implode($glue, $temp).PHP_EOL;
		}

		$filename = $this->type.'_'.date('d.m.Y_h.i.s').'.csv';
		$filename = $this->save($filename);
		if ($filename) {
			echo '<div class="updated"><p>'.__('Export file was created.', ocw_export_locale)
			     .' <a href="'.$filename.'" download="">'.__('Download', ocw_export_locale).',</a> </p></div>';
		} else {
			echo '<div class="error"><p>'.__('Error creating export file', ocw_export_locale).'</p></div>';
		}
	} else if (isset($_POST['remove']) && ! empty($_POST['remove'])) {
		if ($this->removeFile($_POST['file'])) {
			echo '<div class="updated"><p>'.sprintf(__('File %s was removed', ocw_export_locale), $_POST['file']).'</p></div>';
		} else {
			echo '<div class="error"><p>'.__('Error removing file', ocw_export_locale).'</p></div>';
		}
	}

	$directory = $this->readDirectory();
?>
	<div class="ocw_export_users_wrap wrap">
		<div class="title">
			<h2>
				<?=get_admin_page_title()?>
			</h2>
		</div>
		<div class="files">
			<?php
				if ( ! empty($directory)) {
					$form_i = 0;
					foreach ($directory as $file) {
						$form_i++;
						$stat = stat($file);
						$date = new DateTime();
						$date->setTimestamp($stat['atime']);
						$file = basename($file);
						?>
						<form action="" method="post" id="form<?=$form_i?>">
							<input type="hidden" name="file" value="<?=$file?>" />
							<input type="submit" class="dashicons-dismiss dashicons-before" name="remove" value="<?=__('Remove',ocw_export_locale)?>">
							<a
									href="<?=$this->get_download_link($file)?>"
									download=""
									class="fa-file"
							><?=$file?></a>
							<?=' -- '.$this->format_size($stat['size']).' -- '.$date->format('d.m.Y h:i:s').'<br/>'?>
						</form>
						<?php

					}
				}
			?>
		</div>
		<hr />
		<form action="" method="post" class="ocw_export_users_form" id="ocw_export_users_form">
			<!--			Glue:
						<select name="glue" id="">
							<option selected value=";"> ;</option>
							<option value=","> ,</option>
						</select><br />-->
			<input type="submit" value="<?=__('Export',ocw_export_locale)?>" name="save_orders" />
		</form>
		<div></div>
	</div>
<?