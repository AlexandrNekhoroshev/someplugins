<?php
	/**
	 * @var $this ocw_export
	 */
	if (isset($_POST['save_users']) && ! empty($_POST['save_users'])) {
		$glue = (isset($_POST['glue']) && ! empty($_POST['glue']) ? $_POST['glue'] : ';');
		$glue = chr(0x09).chr(0x00);

		$after = $before = '';
		if (isset($_POST['after']) && ! empty($_POST['after'])) {
			$after = $_POST['after'];
		}
		if (isset($_POST['before']) && ! empty($_POST['before'])) {
			$before = $_POST['before'];
		}

		$users_args = [
			'role'       => 'customer',
			'fields'     => 'ID',
			'date_query' => [
				'after'     => $after,
				'before'    => $before,
				'inclusive' => true
			]
		];
		$users      = get_users($users_args);

		$fields = [
			'nickname'            => 'Username',
			'first_name'          => 'First name',
			'last_name'           => 'Last name',
			'b_day'               => 'Day of birth',
			'b_month'             => 'Month of birth',
			'b_year'              => 'Year of birth',
			'billing_first_name'  => 'billing_first_name',
			'billing_last_name'   => 'billing_last_name',
			'billing_phone'       => 'billing_phone',
			'billing_email'       => 'billing_email',
			'billing_address_1'   => 'billing_address_1',
			'billing_house'       => 'billing_house',
			'billing_apartment'   => 'billing_apartment',
			'billing_city'        => 'billing_city',
			'billing_country'     => 'billing_country',
			'billing_postcode'    => 'billing_postcode',
			'shipping_first_name' => 'shipping_first_name',
			'shipping_last_name'  => 'shipping_last_name',
			'shipping_phone'      => 'shipping_phone',
			'shipping_email'      => 'shipping_email',
			'shipping_address_1'  => 'shipping_address_1',
			'shipping_house'      => 'shipping_house',
			'shipping_apartment'  => 'shipping_apartment',
			'shipping_city'       => 'shipping_city',
			'shipping_country'    => 'shipping_country',
			'shipping_postcode'   => 'shipping_postcode'
		];

//		$this->msg = [];
		$this->msg = implode($glue, $fields).PHP_EOL;

		foreach ($users as $user) {
			$user_meta = get_user_meta($user);
			$temp      = [];
			foreach ($fields as $key => $field) {
				if (isset($user_meta[ $key ])) {
					$temp[] = $user_meta[ $key ][0];
				} else {
					$temp[] = '';
				}
				end($temp);
				$key          = key($temp);
				$temp[ $key ] = '"'.$temp[ $key ].'"';
			}
			$this->msg .= implode($glue, $temp).PHP_EOL;
		}
		//$this->msg = hebrev($this->msg);

		$filename = $this->type.'_'.date('d.m.Y_h.i.s').'.csv';
		$filename = $this->save($filename);
		if ($filename) {
			echo '<div class="updated"><p>'.__('Export file was created.',ocw_export_locale)
			     .' <a href="'.$filename.'" download="">'.__('Download',ocw_export_locale).'</a> </p></div>';
		} else {
			echo '<div class="error"><p>Error creating export file</p></div>';
		}
	} else if (isset($_POST['remove']) && ! empty($_POST['remove'])) {
		if ($this->removeFile($_POST['file'])) {
			echo '<div class="updated"><p>'.sprintf(__('File %s was removed', ocw_export_locale), $_POST['file']).'</p></div>';
		} else {
			echo '<div class="error"><p>'.__('Error removing file', ocw_export_locale).'</p></div>';
		}
	}

	$directory = $this->readDirectory();

?>
	<div class="ocw_export_users_wrap wrap">
		<div class="title">
			<h2>
				<?=get_admin_page_title()?>
			</h2>
		</div>
		<div class="files">
			<?php
				if ( ! empty($directory)) {
					foreach ($directory as $file) {
						$stat = stat($file);
						$date = new DateTime();
						$date->setTimestamp($stat['atime']);
						$file = basename($file);
						?>
						<form action="" method="post">
							<input type="hidden" name="file" value="<?=$file?>" />
							<input type="submit" name="remove" value="Remove">
							<a
									href="<?=$this->get_download_link($file)?>"
									download=""
									class="fa-file"
							><?=$file?></a>
							<?=' -- '.$this->format_size($stat['size']).' -- '.$date->format('d.m.Y h:i:s').'<br/>'?>
						</form>
						<?php
//						echo 'x - '.basename($file).' -- '.$stat['size'].' -- '.$date->format('d.m.Y h:i:s').'<br/>';

					}
				}
			?>
		</div>
		<div class="create">
			<hr />
			<form action="" method="post" class="ocw_export_users_form" id="ocw_export_users_form">
				<?=__('Registration date between',ocw_export_locale)?> <input id="from" type="text" class="datepicker" name="after"> - <input id="to" type="text" class="datepicker" name="before"><br />
<!--				Glue:-->
<!--				<select name="glue" id="">-->
<!--					<option selected value=";"> ;</option>-->
<!--					<option value=","> ,</option>-->
<!--				</select><br />-->
				<input type="submit" value="<?=__('Create new export file',ocw_export_locale)?>" class="button button-primary" name="save_users" />
			</form>

		</div>


	</div>


<?php
