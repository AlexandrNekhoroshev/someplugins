<?php
//include_once '../../../wp-load.php';
//die('uncomment line 12 - for safety');



global $woocommerce;
global $wpdb;
error_reporting( E_ALL );
ini_set( 'display_errors', true );
ini_set( 'display_startup_errors', true );
if ( ! function_exists( 'media_handle_upload' ) ) {
  require_once( ABSPATH . "wp-admin" . '/includes/image.php' );
  require_once( ABSPATH . "wp-admin" . '/includes/file.php' );
  require_once( ABSPATH . "wp-admin" . '/includes/media.php' );
}

include_once get_template_directory(). '/include/importer/AbstractImporter.php';
include_once get_template_directory(). '/include/importer/ExcelModel.php';
include_once get_template_directory(). '/include/importer/ProductsImporter.php';
include_once get_template_directory(). '/include/importer/ImporterExistedProduct.php';
include_once get_template_directory(). '/include/importer/ProductTranslationsSyncronizer.php';
include_once get_template_directory(). '/include/importer/ImporterFactory.php';
include_once get_template_directory(). '/include/importer/TranslationExistsException.php';
include_once get_template_directory(). '/include/importer/DuplicateSKUException.php';
include_once get_template_directory(). '/include/importer/NoDataException.php';

$errors = [];

$files_imported_skus = [];
if(isset($_GET['action']) && $_GET['action'] == 'import'){
  //var_dump($_SESSION); die;
  if(isset($_SESSION['ocw_export_success']['files']) && !empty($_SESSION['ocw_export_success']['files'])) {

    include_once(plugin_dir_path(dirname(__FILE__)) . 'excel/Classes/PHPExcel/IOFactory.php');

    $import_results['count_products'] = 0;
    foreach ($_SESSION['ocw_export_success']['files'] as $uploaded_file) {


      $objPHPExcel = PHPExcel_IOFactory::load($uploaded_file);
      $objPHPExcelData = new ExcelModel($objPHPExcel);

      $objPHPExcelData->setExcelStructure([
        'sku' => 0,
        'lang' => 1,
        'cat' => 2,
        'sub_cat' => 3,
        'title' => 4,
        'description' => 5,
        'default' => 6,
        'color' => 7,
        'size' => 8,
        'stock' => 9,
        'price' => 10,
        'sale' => 11,
        'material' => 12,
        'mida' => 13,
        'centimeter' => 14,
        'model' => 15,
        'laundry' => 16,
      ]);

      //$importer = new ProductsImporter($objPHPExcelData, 'he');
      $importer_factory = new ImporterFactory($objPHPExcelData, 'he');
      $importer_factory->setDebugMode(1);

      $main_products_skus = $objPHPExcelData->getMainProductsSkus();
      $files_imported_skus[$uploaded_file] = $main_products_skus;
      $import_data = [];

      foreach ($main_products_skus as $sku) {
        // Create WC_Product
        if(!$sku){ continue; }

        try{
          $importer = $importer_factory->getImporter($sku);
          if(!method_exists($importer, 'run')){
            continue;
          }
          $res = $importer->run();
          if(!$res){
            $errors[$sku] = $importer->getErrors();
          }
          $import_data[$sku] = $importer->products_results;
        }catch(NoDataException $e){
          $errors['fatal_errors'][] = $e->getMessage();
          continue;
        }catch(DuplicateSKUException $e){
          $errors['fatal_errors'][] = $e->getMessage();
          continue;
        }catch(TranslationExistsException $e){
          $import_data[$sku] = $e->getData();
          $import_data[$sku]['status'] = 'translated';
          continue;
        }catch(Exception $e){
          $errors['fatal_errors'][] = $e->getMessage();
          if(isset($importer)){
            $import_data[$sku] = $importer->products_results;
            $import_data[$sku]['status'] = false;
          }
          continue;
        }
        // no errors, just get product data
        $import_data[$sku] = $importer->products_results;



      }

      //echo $importer->getErrorsHtml();
      unset($_SESSION['ocw_export_success']['files']);
    }


    $translation_errors = [];
    foreach($errors as $sku => $sku_errors){
      if(isset($sku_errors['translation_errors'])){
        foreach($sku_errors['translation_errors'] as $lang => $sku_translation_errors){
          foreach($sku_translation_errors as $term => $term_translation_error){
            $translation_errors[$lang][$term] = $term_translation_error;
          }
        }
        unset($errors[$sku]['translation_errors']);
      }
    }

  }
}

?>
<h1>Import products</h1>

<?php if(isset($_SESSION['ocw_import_images']) && is_array($_SESSION['ocw_import_images'])): ?>
  <h2>Images uploading finished</h2>
  <?php foreach($_SESSION['ocw_import_images'] as $file_data):?>
    <div><?=$file_data['file']?>: <?=$file_data['status']?></div>
  <?php endforeach;?>
  <?php unset($_SESSION['ocw_import_images'])?>
<?php endif; ?>

<?php if(isset($import_results['count_products']) && $import_results['count_products']):?>
  <strong>
    <?php printf(__('%d products was imported.', 'ocw_export'), $import_results['count_products'])?>
  </strong>
<?php endif;?>

<div class="upload-container">
  <input type="file" multiple="" id="ocw_import_files">
  <br>
  <div id="zip-progress-container" class="hidden">
    <strong>Creating archive</strong>
    <div class="zip-progress-current">
      Current file: <span id='current-file'></span><br>
      <progress value="0" id='current-file-progress' style="position: static"></progress>
    </div>
    <div class="zip-progress-total">
      Total: <span id='total'></span><br>
      <progress value="0" id='total-progress' style="position: static"></progress>
    </div>
  </div>
  <div id="upload-progress-container" class="hidden">
    <strong>Uploading archive</strong><br>
    <progress value="0" id='upload-progress' style="position: static"></progress>
  </div>
  <div id="extract-progress-container" class="hidden">
    <strong>Extracting archive</strong><br>
    <span id="extract-progress-text">Extracting</span>
    <progress id='extract-progress' style="position: static"></progress>
  </div>
  <br>
  <button type="button" id="download-button" class="button button-primary">Upload</button>
</div>


<form name="ocw_import" method="POST" action="<?= admin_url('admin-post.php')?>"  enctype="multipart/form-data">
  <?php wp_nonce_field() ?>
  <input type="hidden" name="action" value="ocw-import-from-xlsx">

  <h2><?= __('Select files to import', 'ocw_export');?></h2>
  <input id="ocw_import_files" type="file" name="ocw_import_files[]" multiple  accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
  <?php submit_button('Import') ?>

  <br>
</form>

<?php /*  var_dump($errors);*/ ?>
<?php ?>
<?php if(isset($errors)): ?>
  <div class="errors-container">

    <?php /* -- Fatal errors -- */?>
    <?php if(isset($errors['fatal_errors'])):?>
      <h3 class="fatal-errors-caption">Fatal errors:</h3>
      <?php foreach($errors['fatal_errors'] as $error):?>
        <div class="fatal-error">
          &emsp;<?=$error?>
        </div>
      <?php endforeach;?>
    <?php endif;?>
    <?php /* -- end/Fatal errors -- */?>

    <?php foreach($errors as $sku => $sku_errors):?>
      <?php if(isset($sku_errors['product_translation_exists'])): ?>
        <div class="translations-products-errors-container">
          <?php foreach($sku_errors['product_translation_exists'] as $lang => $trannslation_prodict_error):?>
            <div>
              Translation for product <?= $sku ?> already exists for <strong><?= $lang ?></strong> language.<br>
              Check the correctness of existing translation.
              <?=$trannslation_prodict_error?>
            </div>
          <?php endforeach;?>
        </div>
      <?php endif;?>

    <?php endforeach;?>
  </div>
<?php endif;?>


<?php /* -- Translation errors -- */?>
<?php if(!empty($translation_errors)): ?>
<div class="translations-errors-container">
  <h3>Next product attributes has no translations:</h3>

  <?php foreach($translation_errors as $lang => $term_names):?>
    <div class="translation-error-lang">
      &emsp;No translations for language <strong><?=$lang?></strong> for next attributes:
    </div>
    <?php foreach($term_names as $term_error):?>
      <div class="translation-error">
        &emsp;&emsp;<?=$term_error?>
      </div>
    <?php endforeach;?>
  <?php endforeach;?>
  <br>
  <div><strong>Add translations to specified product attributes and run import again</strong></div>
</div>
<?php /* -- end/Translation errors -- */?>
<?php endif;?>

<style>
  .product_import_thumb{
    display: inline-block;
    width: 150px;
    height: 150px;
    position: relative;
    border: 1px solid #000;
    margin: 2px;
    vertical-align: top;
    align: center;
  }

  .import_result_icon{
    position: absolute;
    bottom: 0;
    right: 0;
    width: 32px;
    height: 32px;
  }

  .import_result_icon.status-ok{
    background: URL("<?=WP_PLUGIN_URL.'/'.ocw_export_locale.'/assets/img/Check-icon.png'?>");
  }
  .import_result_icon.status-fail{
    background: URL("<?=WP_PLUGIN_URL.'/'.ocw_export_locale.'/assets/img/Close-2-icon.png'?>");
  }
  .import_result_icon.status-translated{
    background: URL("<?=WP_PLUGIN_URL.'/'.ocw_export_locale.'/assets/img/translated1.png'?>");
  }


  .product_import_thumb img{
    width: 100%;
    height: 100%;
  }
  .product-info-container{
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    background-color: rgba(0,0,0,0.75);
    color: #FFF;
    text-align: center;
    cursor: pointer;
  }
  .import-title{
    color: #fff;
    font-weight: 700;
  }
  .import-error{
    color: #D70000;
    font-weight: 700;
  }
</style>
<?php if(!empty($import_data)):?>

  <div class="product_results_container">
    <?php foreach($import_data as $sku => $sku_data):?>
      <?php switch($sku_data['status']){
        case 'translated': $class = 'status-translated'; break;
        case 'ok':         $class = 'status-ok'; break;
        case false:        $class = 'status-fail'; break;
      }?>
      <div class="product_import_thumb">
        <?php if(isset($sku_data['thumb'])):?>
          <img src="<?=$sku_data['thumb'];?>">
        <?php else:?>
          <?=$sku;?>
        <?php endif;?>
        <div class="import_result_icon <?=$class?>"></div>
        <div class="product-info-container hidden">
          <a href="<?=get_the_permalink($sku_data['id'])?>">
          <p class="import-title">
            <?=$sku_data['title']?>
          </p>
          <?php if(isset($sku_data['error'])):?>
            <p class="import-error">
              <?=$sku_data['error']?>
            </p>
          <?php endif; ?>
          </a>
        </div>
      </div>
    <?php endforeach;?>
  </div>
<?php endif;?>
<script>
  jQuery(function($){
    $('.product_import_thumb').hover(
      function(){$(this).find('.product-info-container').show()},
      function(){$(this).find('.product-info-container').hide()}
    );
    $('#submit').on('click', function () {
      var $this = $(this);
      $this.addClass('disabled');
      //$this.attr('disabled', true);
    });
  });
</script>
