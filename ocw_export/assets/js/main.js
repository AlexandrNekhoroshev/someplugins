jQuery(document).ready(function ($) {
  $('#ocw_import_files, #ocw_import_images').styler({
    locale: 'en'
  });


  var dateFormat = "dd-mm-yy",
		from = $("#from")
			.datepicker({
				defaultDate: "+1w",
				changeMonth: true,
				numberOfMonths: 1,
				dateFormat: dateFormat
			})
			.on("change", function () {
				to.datepicker("option", "minDate", getDate(this));
			}),
		to = $("#to").datepicker({
			defaultDate: "+1w",
			changeMonth: true,
			numberOfMonths: 1,
			maxDate: 0,
			dateFormat: dateFormat
		})
			.on("change", function () {
				from.datepicker("option", "maxDate", getDate(this));
			});

	function getDate(element) {
		var date;
		try {
			date = $.datepicker.parseDate(dateFormat, element.value);
		} catch (error) {
			date = null;
		}

		return date;
	}
});
