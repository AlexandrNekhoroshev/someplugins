console.log(zip.workerScriptsPath);

(function(obj) {

	var requestFileSystem = obj.webkitRequestFileSystem || obj.mozRequestFileSystem || obj.requestFileSystem;

	function onerror(message) {
		alert(message);
	}

	function createTempFile(callback) {
		var tmpFilename = "tmp.zip";
		requestFileSystem(TEMPORARY, 4 * 1024 * 1024 * 1024, function(filesystem) {
			function create() {
				filesystem.root.getFile(tmpFilename, {
					create : true
				}, function(zipFile) {
					callback(zipFile);
				});
			}

			filesystem.root.getFile(tmpFilename, null, function(entry) {
				entry.remove(create, create);
			}, create);
		});
	}

	var model = (function() {
		var zipFileEntry, zipWriter, writer, creationMethod, URL = obj.webkitURL || obj.mozURL || obj.URL;

		return {
			setCreationMethod : function(method) {
				creationMethod = method;
			},
			addFiles : function addFiles(files, oninit, onadd, onprogress, onend) {
				var addIndex = 0;

				function nextFile() {
					var file = files[addIndex];
					onadd(file);
					zipWriter.add(file.name, new zip.BlobReader(file), function() {
						addIndex++;
						if (addIndex < files.length)
							nextFile();
						else
							onend();
					}, onprogress);
				}

				function createZipWriter() {
					zip.createWriter(writer, function(writer) {
						zipWriter = writer;
						oninit();
						nextFile();
					}, onerror);
				}

				if (zipWriter)
					nextFile();
				else if (creationMethod == "Blob") {
					writer = new zip.BlobWriter();
					createZipWriter();
				} else {
					createTempFile(function(fileEntry) {
						zipFileEntry = fileEntry;
						writer = new zip.FileWriter(zipFileEntry);
						createZipWriter();
					});
				}
			},
			getBlob : function(callback) {
				zipWriter.close(callback);
			}
		};
	})();

	(function() {
		var fileInput = document.getElementById("ocw_import_files");
		var zipProgress = document.createElement("progress");
		var downloadButton = document.getElementById("download-button");
		var filenameInput = document.getElementById("filename-input");

		var currentFile = document.getElementById("current-file");
		var currentFileProgress = document.getElementById("current-file-progress");
		var totalFilesProgress = document.getElementById("total-progress");
		var totalFilesProgressPercent = document.getElementById("total");
		var progress_container = document.getElementById("zip-progress-container");
		var uploadProgressContainer = document.getElementById('upload-progress-container');
		var uploadProgressIndicator = document.getElementById('upload-progress');

    var extractProgressContainer = document.getElementById('extract-progress-container');
    var extractProgressText = document.getElementById('extract-progress-text');

		model.setCreationMethod('Blob');
		fileInput.addEventListener('change', function() {
			fileInput.disabled = true;
		}, false);

		downloadButton.addEventListener("click", function(event) {
			var num_files = fileInput.files.length;
			if(num_files == 0){
				return;
			}
			var current_num = 0;

			totalFilesProgress.max = num_files;

			if('application/zip' == fileInput.files[0].type){
				var formData = new FormData();
				formData.append('test', fileInput.files[0]);
				formData.append('action', 'zip-upload');
				uploadProgressContainer.classList.remove('hidden');

				var request = new XMLHttpRequest();
				request.upload.onprogress = function(event){
					uploadProgressIndicator.value = event.loaded;
					uploadProgressIndicator.max = event.total;

				};
        request.upload.onload = function(data){
          extractProgressContainer.classList.remove('hidden');
        };
        request.onreadystatechange = function() {
          if (request.readyState === 4) {
            var res_data = JSON.parse(request.response);
            if(res_data.success == true){
              extractProgressText.innerHTML = 'Extraction finished';
            }else{
              extractProgressText.innerHTML = 'Error: ' + res_data.data;
            }
            document.getElementById('extract-progress').remove();
          }
        };
				request.open("POST", ajaxurl);
				request.send(formData);
				return	false;
			}else{
				totalFilesProgressPercent.textContent = '0%';
				progress_container.classList.remove('hidden');
				model.addFiles(fileInput.files, function() {
				}, function(file) {
					currentFileProgress.value = 0;
					currentFileProgress.max = 0;
					currentFile.textContent = file.name;
					totalFilesProgress.value = ++current_num;
					totalFilesProgressPercent.textContent = Math.round(current_num/num_files*100) + '%';

				}, function(current, total) {
					currentFileProgress.value = current;
					currentFileProgress.max = total;
				}, function() {
					fileInput.value = "";
					fileInput.disabled = false;
					progress_container.querySelector('.zip-progress-current').classList.add('hidden');
					currentFile.textContent = '';
					uploadProgressContainer.classList.remove('hidden');
					model.getBlob(function(blob) {
						var formData = new FormData();
						formData.append('test', blob);
						formData.append('action', 'zip-upload');

						var request = new XMLHttpRequest();
						request.upload.onprogress = function(event){
							uploadProgressIndicator.value = event.loaded;
							uploadProgressIndicator.max = event.total;
						};
            request.onreadystatechange = function() {
              if (request.readyState === 4) {
                var res_data = JSON.parse(request.response);
                if(res_data.success == true){
                  extractProgressText.innerHTML = 'Extraction finished';
                }else{
                  extractProgressText.innerHTML = 'Error: ' + res_data.data;
                }
                document.getElementById('extract-progress').remove();
              }
            };
						request.upload.onload = function(){
              extractProgressContainer.classList.remove('hidden');
						};
						request.open("POST", ajaxurl);
						//console.log(formData);
						request.send(formData);
						return	false;

					});
				});
			}

		}, false);

	})();

})(this);
