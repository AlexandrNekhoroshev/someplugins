<?php
/**
 * Plugin Name: Export users, orders
 * Description: Export users, orders
 * Version: 1.0.0
 * Author: Elidone
 *
 * @package  WooCommerce WordPress
 * @category Core
 * @author   Elidone
 */

if ( ! class_exists( 'ocw_export' ) ) {
  define( 'NL', chr( 0x0A ) . chr( 0x00 ) );
  define( 'ocw_export_locale', 'ocw_export' );

  /**
   * Class ocw_export
   *
   * @var fsafsaf fsafsaf
   */
  class ocw_export {
    private $folder = '';
    private $backup_folder = 'backup/';
    private $dir_status = 0;
    private $msg = '';
    private $type = '';
    private $plugin_folder = '';
    private $plugin_uri = '';
    private $units = [ 'B', 'KB', 'MB', 'GB', 'TB', 'PB' ];


    /**
     * ocw_export constructor.
     */
    public function __construct() {
      $this->init();
    }

    public function ocw_export_users() {
      $this->type = 'users';
      $this->load_view();
    }

    public function ocw_export_orders() {
      $this->type = 'orders';
      $this->load_view();
    }

    public function ocw_export_products() {
      $this->type = 'products';
      $this->load_view();
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///                                                                                                 ///
    ///         Ajax                                                                                    ///
    ///                                                                                                 ///
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///                                                                                                 ///
    ///         Actions                                                                                 ///
    ///                                                                                                 ///
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    public function admin_menu_action() {

      global $admin_page_hooks;

      if ( isset( $admin_page_hooks['ocw-options'] ) ) {
        add_submenu_page(
          'ocw-options',  //or 'options.php'
          __( 'Export users', ocw_export_locale ),
          __( 'Export users', ocw_export_locale ),
          'manage_options',
          'ocw-export-users',
          [ $this, 'ocw_export_users' ]
        );

        add_submenu_page(
          'ocw-options',  //or 'options.php'
          __( 'Export orders', ocw_export_locale ),
          __( 'Export orders', ocw_export_locale ),
          'manage_options',
          'ocw-export-orders',
          [ $this, 'ocw_export_orders' ]
        );
        add_submenu_page(
          'ocw-options',  //or 'options.php'
          __( 'Import products', ocw_export_locale ),
          __( 'Import products', ocw_export_locale ),
          'manage_options',
          'ocw-export-products',
          [ $this, 'ocw_export_products' ]
        );
      } else {
        add_menu_page(
          'OCW Plugins',
          'OCW Plugins',
          'manage_options',
          'ocw-options',
          [ $this, 'ocw_export_settings' ],
          '',
          4
        );

        add_submenu_page(
          'ocw-options',  //or 'options.php'
          __( 'Export users', ocw_export_locale ),
          __( 'Export users', ocw_export_locale ),
          'manage_options',
          'ocw-export-users',
          [ $this, 'ocw_export_users' ]
        );

        add_submenu_page(
          'ocw-options',  //or 'options.php'
          __( 'Export orders', ocw_export_locale ),
          __( 'Export orders', ocw_export_locale ),
          'manage_options',
          'ocw-export-orders',
          [ $this, 'ocw_export_orders' ]
        );

        add_submenu_page(
          'ocw-options',  //or 'options.php'
          __( 'Import products', ocw_export_locale ),
          __( 'Import products', ocw_export_locale ),
          'manage_options',
          'ocw-export-products',
          [ $this, 'ocw_export_products' ]
        );
      }

    }

    public function admin_enqueue_scripts() {
      $current_screen = get_current_screen();
      if($current_screen->id == 'ocw-plugins_page_ocw-export-products'){
        wp_register_style('formstyler', $this->plugin_uri .'/assets/formstyler/jquery.formstyler.css');
        wp_register_style('formstyler-theme', $this->plugin_uri .'/assets/formstyler/jquery.formstyler.theme.css');
        wp_register_script('formstyler', $this->plugin_uri  . '/assets/formstyler/jquery.formstyler.js', [ 'jquery' ], false, true );

        wp_register_script('zip', $this->plugin_uri  . '/assets/uploader/lib/zip.js', [],  false, true);
        wp_add_inline_script('zip', 'zip.workerScriptsPath = "'.$this->plugin_uri.'assets/uploader/lib/";');

        wp_register_script('zip-ext', $this->plugin_uri  . '/assets/uploader/lib/zip-ext.js', ['zip'],  false, true);
        //wp_localize_script('zip-ext', 'zip', ['workerScriptsPath' => "lib/"]);
        wp_register_script('zip-uploader', $this->plugin_uri  . '/assets/uploader/uploader.js', ['zip-ext'],  false, true);

        wp_enqueue_script('formstyler');
        wp_enqueue_style('formstyler');
        wp_enqueue_style('formstyler-theme');

        wp_enqueue_script('zip');
        wp_enqueue_script('zip-ext');
        wp_enqueue_script('zip-uploader');

      }

      $page = isset($_GET['page']) ? $_GET['page'] : '' ;
      if(strpos($_GET['page'],'ocw') !== false && $_GET['page'] != 'ocw-discounts'){

        //wp_enqueue_script( 'jquery-ui', $this->plugin_uri . 'assets/jquery/jquery-ui.min.js', array( 'jquery' ), '', true );
        wp_enqueue_script( 'ocw_export', $this->plugin_uri . 'assets/js/main.js', ['jquery-ui-datepicker'] );

        wp_enqueue_style( 'jquery-ui-style', $this->plugin_uri . 'assets/jquery/jquery-ui.min.css' );
        wp_enqueue_style( 'jquery-ui-scructure', $this->plugin_uri . 'assets/jquery/jquery-ui.structure.min.css' );
        wp_enqueue_style( 'jquery-ui-theme', $this->plugin_uri . 'assets/jquery/jquery-ui.theme.min.css' );
      }
    }


    public function ocw_export_settings() {
      global $admin_page_hooks, $submenu;

      foreach ( $GLOBALS['ocw_plugins'] as $val ) {
        echo $val . '<br/>';
      }

      ?>
      <div class="wrap">
        <h2><?php echo get_admin_page_title() ?></h2>


      </div>
      <?php
    }



    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///                                                                                                 ///
    ///         Filters                                                                                 ///
    ///                                                                                                 ///
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    ///                                                                                                 ///
    ///         Private                                                                                 ///
    ///                                                                                                 ///
    ///////////////////////////////////////////////////////////////////////////////////////////////////////

    private function init() {
      $this->folder        = plugin_dir_path( __FILE__ ) . $this->backup_folder;
      $this->plugin_folder = plugin_dir_path( __FILE__ );
      $this->plugin_uri    = plugin_dir_url( __FILE__ );
      $this->checkDirectory();
      add_action( 'admin_menu', [ $this, 'admin_menu_action' ] );
      add_action( 'admin_enqueue_scripts', [ $this, 'admin_enqueue_scripts' ] );
    }

    private function save( $filename ) {
      if ( $this->dir_aviable() ) {
        $fp = fopen( $this->folder . $filename, 'w+' );
        fputs( $fp, chr( 0xFF ) . chr( 0xFE ) );
        $this->msg = mb_convert_encoding( $this->msg, 'UTF-16LE', 'UTF-8' );
        fwrite( $fp, $this->msg );
        fclose( $fp );

        return $this->get_download_link( $filename );
      }

      return false;

    }

    private function utf2he( $he ) {
      return '"' . mb_convert_encoding( $he, 'UTF-16LE', 'UTF-8' ) . '"';
    }

    private function checkDirectory() {
      if ( ! file_exists( $this->folder ) ) {
        if ( is_dir( $this->folder ) ) {
          $this->dir_status = 1;
        } else {
          if ( mkdir( $this->folder ) ) {
            $this->dir_status = 1;
          }
        }
      } else {
        if ( is_dir( $this->folder ) ) {
          $this->dir_status = 1;
        }
      }
    }

    private function dir_aviable() {
      if ( $this->dir_status && is_writeable( $this->folder ) ) {
        return true;
      } else {
        return false;
      }
    }

    private function readDirectory() {
      if ( $this->dir_status ) {
        return glob( $this->folder . $this->type . '*' );
      }
    }

    private function removeFile( $file ) {
      if ( $this->dir_status ) {
        if ( file_exists( $this->folder . $file ) ) {
          if ( unlink( $this->folder . $file ) ) {
            return true;
          } else {
            return false;
          }

        }

        return false;
      }

      return false;
    }

    private function load_view( $type = '', $folder = '' ) {
      if ( empty( $type ) ) {
        $type = $this->type;
      }
      if ( ! empty( $folder ) ) {
        $folder .= '/';
      }
      if ( file_exists( $this->plugin_folder . 'view/' . $folder . $type . '.php' ) ) {
        require_once $this->plugin_folder . 'view/' . $folder . $type . '.php';
      }
    }

    private function get_download_link( $filename ) {
      return plugin_dir_url( __FILE__ ) . $this->backup_folder . $filename;
    }

    private function format_size( $size ) {
      $mod = 1024;

      for ( $i = 0; $size > $mod; $i ++ ) {
        $size /= $mod;
      }

      $endIndex = strpos( $size, "." ) + 3;

      return substr( $size, 0, $endIndex ) . ' ' . $this->units[ $i ];
    }
  }

  add_action( 'plugins_loaded', function () {
    if ( is_admin() ) {
      $GLOBALS['ocw_export']    = new ocw_export();
      $GLOBALS['ocw_plugins'][] = 'ocw_export';
    }
  } );

  if ( is_admin() ) {
    add_action( 'plugins_loaded', 'ocw_export_init' );
    add_action( 'init', 'init_ocw_export_locale' );
  }
  function ocw_export_init() {
    load_plugin_textdomain( 'ocw_export', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
  }

  function init_ocw_export_locale() {
    load_textdomain( 'ocw_export', dirname( plugin_basename( __FILE__ ) ) . 'lang/ocw_export-' . get_locale() . '.mo' );
  }

  add_action('admin_post_ocw-import-from-xlsx', function(){

    if(!wp_verify_nonce($_POST['_wpnonce'])){
      $_SESSION['ocw_export_errors'][] = 'Bad token';
    }

    // handle tables uploading
    if(isset($_FILES['ocw_import_files']) && !empty($_FILES['ocw_import_files'])){
      $images_files = $_FILES['ocw_import_files'];
      $images_count = count($images_files['name']);

      for($i=0; $i<$images_count; $i++){
        $file = [
          'name'       => $images_files['name'][$i],
          'type'       => $images_files['type'][$i],
          'tmp_name'   => $images_files['tmp_name'][$i],
          'error'      => $images_files['error'][$i],
          'size'       => $images_files['size'][$i]
        ];

        $overrides = array(
          'test_form'  => false,
        );

        $movefile = wp_handle_upload( $file, $overrides );

        if ($movefile && empty($movefile['error']) ) {
          $_SESSION['ocw_export_success']['files'][] = $movefile['file'];
        } else {
          $_SESSION['ocw_export_errors']['files'] = $file['name'];
        }
      }
    }

    wp_redirect($_POST['_wp_http_referer'].'&action=import'); die;
  });

  add_action('admin_post_ocw-import-images', function(){

    if(isset($_SESSION['ocw_import_images'])){
      unset($_SESSION['ocw_import_images']);
    }

    if(!wp_verify_nonce($_POST['_wpnonce'])){
      $_SESSION['ocw_export_errors'][] = 'Bad token';
    }

    // Upload location modification
    add_filter('upload_dir', function($data){
      $folder = 'ocw_import';
      $path = array(
        'path' => WP_CONTENT_DIR . '/uploads/' .$folder,
        'url' => WP_CONTENT_URL . '/uploads/' .$folder,
        'subdir' => $folder,
        'basedir' => WP_CONTENT_DIR . '/uploads',
        'baseurl' => WP_CONTENT_URL . '/uploads',
        'error' => false,
      );

      return $path;
    });

    // handle images uploading
    if(isset($_FILES['ocw_import_images']) && !empty($_FILES['ocw_import_images'])){

      $images_files = $_FILES['ocw_import_images'];
      $images_count = count($images_files['name']);

      for($i=0; $i<$images_count; $i++){
        $file = [
          'name'       => $images_files['name'][$i],
          'type'       => $images_files['type'][$i],
          'tmp_name'   => $images_files['tmp_name'][$i],
          'error'      => $images_files['error'][$i],
          'size'       => $images_files['size'][$i]
        ];
        $overrides = array(
          'test_form'  => false,
        );
        $movefile = wp_handle_upload( $file, $overrides );

        if ($movefile && empty($movefile['error']) ) {
          $_SESSION['ocw_import_images'][] = ['file' => $file['name'], 'status' => 'ok'];
        } else {
          $_SESSION['ocw_import_images'][] = ['file' => $file['name'], 'status' => 'fail'];
        }
      }
    }
    wp_redirect($_POST['_wp_http_referer'].'&action=upload'); die;
  });

  add_action('wp_ajax_zip-upload', function(){
    $zip = new ZipArchive;
    if(!empty($_FILES)){

      // Upload location modification
      add_filter('upload_dir', function($data) {
        $folder = 'ocw_import';
        $data['path'] = WP_CONTENT_DIR . '/uploads/' . $folder;
        $data['subdir'] = $folder;
        $data['basedir'] = WP_CONTENT_DIR . '/uploads';
        return $data;
      });

      $file = $_FILES['test'];

      $name = $_FILES['test']['name'];
      $name_info = pathinfo($name);
      if(!$name_info['extension']){
        $name .= '.zip';
      }
      $file['name'] = $name;


      $overrides = array(
        'test_form'  => false,
      );

      $movefile = wp_handle_upload( $file, $overrides );
      if (!$movefile['error']) {
        if ($zip->open($movefile['file']) === TRUE) {

          $extract = $zip->extractTo(WP_CONTENT_DIR.'/uploads/ocw_import');
          $zip->close();
          unlink($movefile['file']);
          if($extract){
            wp_send_json_success();
          }else{
            wp_send_json_error('Cannot extract archive');
          }

          die;
        } else {
          wp_send_json_error('Cannot open archive');
          die;
        }
      } else {
        wp_send_json_error($movefile['error']);
        die;
      }
    }else{
      wp_send_json_error('No files received');
      die;
    }

  });
}